jQuery(document).ready(function(e) {
    function n(e, t, n, r, i, s, o, u) {
        if (typeof u == "undefined") {
            u = false
        }
        if (typeof s === "undefined") {
            s = 5
        }
        e.beginPath();
        e.moveTo(t + s, n);
        e.lineTo(t + r - s, n);
        e.quadraticCurveTo(t + r, n, t + r, n + s);
        e.lineTo(t + r, n + i - s);
        e.quadraticCurveTo(t + r, n + i, t + r - s, n + i);
        e.lineTo(t + s, n + i);
        e.quadraticCurveTo(t, n + i, t, n + i - s);
        e.lineTo(t, n + s);
        e.quadraticCurveTo(t, n, t + s, n);
        e.closePath();
        if (u) {
            e.strokeStyle = u;
            e.stroke()
        }
        if (o) {
            e.fill()
        }
    }

    function L() {
        w.on("object:selected", function(t) {
            e("#is-curved").removeAttr("checked");
            if (t.target) {
                var n = t.target.type;
                var r = ["rect", "circle", "triangle", "polygon"];
                if (n == "text") {
                    e(".text-btn").click();
                    e("#font-family-selector").val(t.target.get("fontFamily"));
                    e("#font-size-slider").val(t.target.get("fontSize"));
                    e("#txt-color-selector").css("background-color", t.target.get("fill"));
                    e("#txt-bg-color-selector").css("background-color", t.target.get("backgroundColor"));
                    e("#new-text").val(t.target.get("text"));
                    var i = t.target.get("textDecoration");
                    if (i == "underline") e("#underline-cb").attr("checked", "checked");
                    else e("#underline-cb").removeAttr("checked");
                    var s = t.target.get("fontWeight");
                    if (s == "bold") e("#bold-cb").attr("checked", "checked");
                    else e("#bold-cb").removeAttr("checked");
                    var o = e("#selected_object").get("fontStyle");
                    if (o == "italic") e("#italic-cb").attr("checked", "checked");
                    else e("#italic-cb").removeAttr("checked");
                    if (t.target.get("stroke") != false && t.target.getStroke() != null) {
                        e("#txt-outline-color-selector").css("background-color", t.target.get("stroke"));
                        e("#o-thickness-slider").val(t.target.get("strokeWidth"))
                    } else {
                        e("#o-thickness-slider").val(0)
                    }
                    var u = t.target.opacity;
                    e("#opacity-slider").val(u)
                } else if (n == "group") {
                    if (t.target.get("originalText")) {
                        e("#is-curved").attr("checked", "checked");
                        e(".text-btn").click();
                        e("#font-family-selector").val(t.target.item(0).get("fontFamily"));
                        e("#font-size-slider").val(t.target.item(0).get("fontSize"));
                        e("#txt-color-selector").css("background-color", t.target.item(0).get("fill"));
                        e("#txt-bg-color-selector").css("background-color", t.target.item(0).get("backgroundColor"));
                        e("#new-text").val(t.target.get("originalText"));
                        e("#curved-txt-radius-slider").val(t.target.get("radius"));
                        e("#curved-txt-spacing-slider").val(t.target.get("spacing"));
                        var i = t.target.item(0).get("textDecoration");
                        if (i == "underline") e("#underline-cb").attr("checked", "checked");
                        else e("#underline-cb").removeAttr("checked");
                        var s = t.target.item(0).get("fontWeight");
                        if (s == "bold") e("#bold-cb").attr("checked", "checked");
                        else e("#bold-cb").removeAttr("checked");
                        var o = e("#selected_object").get("fontStyle");
                        if (o == "italic") e("#italic-cb").attr("checked", "checked");
                        else e("#italic-cb").removeAttr("checked");
                        if (t.target.item(0).get("stroke") != false && t.target.item(0).getStroke() != null) {
                            e("#txt-outline-color-selector").css("background-color", t.target.item(0).get("stroke"));
                            e("#o-thickness-slider").val(t.target.item(0).get("strokeWidth"))
                        } else {
                            e("#o-thickness-slider").val(0)
                        }
                        var u = t.target.item(0).opacity;
                        e("#opacity-slider").val(u)
                    }
                } else if (jQuery.inArray(n, r) >= 0) {
                    var a = t.target.opacity;
                    e("#shape-opacity-slider").val(a);
                    e("#shape-bg-color-selector").css("background-color", t.target.get("fill"));
                    e("#shape-outline-color-selector").css("background-color", t.target.get("stroke"));
                    e("#shape-thickness-slider").val(t.target.get("strokeWidth"))
                } else if (n == "image") {
                    e(".images-btn").click();
                    var f = t.target.filters;
                    e("#img-effects input:checkbox").removeAttr("checked");
                    e.each(f, function(t, n) {
                        if (n) {
                            var r = n.type;
                            var i = n.matrix;
                            var s = [1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9];
                            var o = [0, -1, 0, -1, 5, -1, 0, -1, 0];
                            var u = [1, 1, 1, 1, .7, -1, -1, -1, -1];
                            if (r == "Grayscale") e("#grayscale").attr("checked", "checked");
                            else if (r == "Invert") e("#invert").attr("checked", "checked");
                            else if (r == "Sepia") e("#sepia").attr("checked", "checked");
                            else if (r == "Sepia2") e("#sepia2").attr("checked", "checked");
                            else if (r == "Convolute") {
                                if (e(i).not(s).length == 0 && e(s).not(i).length == 0) e("#blur").attr("checked", "checked");
                                else if (e(i).not(o).length == 0 && e(o).not(i).length == 0) e("#sharpen").attr("checked", "checked");
                                else if (e(i).not(u).length == 0 && e(u).not(i).length == 0) e("#emboss").attr("checked", "checked")
                            } else console.log(r, i)
                        }
                    })
                } else if ((n == "path" || n == "path-group") && g != "none") {
                    e(".images-btn").click();
                    e("#clipart-bg-color-container").html("");
                    if (t.target.isSameColor && t.target.isSameColor() || !t.target.paths) {
                        var l = "clipart-bg-" + 1 + "-color-selector";
                        var c = '<span id="' + l + '" class="svg-color-selector" data-placement="top" data-original-title="Background color (SVG files only)" style="background-color:' + t.target.get("fill") + '"></span>';
                        e("#clipart-bg-color-container").append(c);
                        e("[data-original-title]").tooltip();
                        M(l)
                    } else if (t.target.paths) {
                        var h = [];
                        var p = 0;
                        for (var d = 0; d < t.target.paths.length; d++) {
                            var l = "clipart-bg-" + p + "-color-selector";
                            var v = t.target.paths[d].fill;
                            var c = '<span id="' + l + '" class="svg-color-selector" data-placement="top" data-original-title="Background color (SVG files only)" style="background-color:' + v + '" data-index="' + d + '"></span>';
                            if (g == "by-colors") {
                                var m = jQuery.inArray(v, h);
                                if (m == -1) {
                                    e("#clipart-bg-color-container").append(c);
                                    e("[data-original-title]").tooltip();
                                    M(l);
                                    h.push(v);
                                    p++
                                } else {
                                    var y = "#clipart-bg-" + m + "-color-selector";
                                    var b = e(y).attr("data-index");
                                    e(y).attr("data-index", b + "," + d)
                                }
                            } else {
                                e("#clipart-bg-color-container").append(c);
                                e("[data-original-title]").tooltip();
                                M(l);
                                p++
                            }
                        }
                    }
                }
                if (t.target.get("lockMovementX")) e("#lock-mvt-x").attr("checked", "checked");
                else e("#lock-mvt-x").removeAttr("checked");
                if (t.target.get("lockMovementY")) e("#lock-mvt-y").attr("checked", "checked");
                else e("#lock-mvt-y").removeAttr("checked");
                if (t.target.get("lockScalingX")) e("#lock-scl-x").attr("checked", "checked");
                else e("#lock-scl-x").removeAttr("checked");
                if (t.target.get("lockScalingY")) e("#lock-scl-y").attr("checked", "checked");
                else e("#lock-scl-y").removeAttr("checked");
                if (t.target.get("lockDeletion")) e("#lock-Deletion").attr("checked", "checked");
                else e("#lock-Deletion").removeAttr("checked")
            }
        });
        w.on("object:added", function(y) {
            if (y.target) {
                w.calcOffset();
                w.renderAll();
                y.target.setCoords();
                var t = y.target.type;
                if (t == "text") {
                    P()
                }
                w.calcOffset()
            }
            AB();
        });
        w.on("object:modified", function(e) {
            w.calcOffset();
            w.renderAll();
            e.target.setCoords();
            Q()
        });
        w.on("mouse:over", function(e) {})
    }

    e('#layer_canvas').on('click','.layer_label',function() {
        //console.log(e(this).data('index'));
        w.setActiveObject(w.item(e(this).data('index')));
    });

    function AB() {
            var t = w.getObjects();
            if(t!=null) {
                e("#layer_canvas").empty();
                e.each(t,function(index,value){
                 if(t[index].text!=null) {
                        var p =  '<div><input type="button" class="layer_btn lock" data-placement="top" data-original-title="Lock the layer"/>';
                        p = p + '<input type="button" class="layer_btn visible" data-placement="top" data-original-title="Hide layer"/>';
                        p = p + '<span class="layer_label" data-index="'+index+'"><label>Text</label></span>';
                        p = p + '<input type="button" class="layer_btn send_back" data-placement="top" data-original-title="Send back"/>';
                        p = p + '<input type="button" class="layer_btn bring_front" data-placement="top" data-original-title="Bring front" data-index="'+index+'"/>';
                        p = p + '<input type="button" class="layer_btn delete" data-placement="top" data-original-title="Delete" data-index="'+index+'"/></div>';

                    e("#layer_canvas").append(p);
                } else if (t[index].name == "Circle") {
                    var p = '<div><input type="button" class="layer_btn lock" data-placement="top" data-original-title="unlock the layer"/>';
                        p = p + '<input type="button" class="layer_btn visible" data-placement="top" data-original-title="hide layer"/>';
                        p = p + ' <span class="layer_label" data-index="'+index+'"><label>Circle</label> </span>';
                        p = p + '<input type="button" class="layer_btn send_back" data-placement="top" data-original-title="send back"/>';
                        p = p + '<input type="button" class="layer_btn bring_front" data-placement="top" data-original-title="bring front" data-index="'+index+'"/>';
                        p = p + '<input type="button" class="layer_btn delete" data-placement="top" data-original-title="Delete"  data-index="'+index+'"/></div>';
                    e("#layer_canvas").append(p);
                } else if (t[index].name == "R Square") {
                    var p = '<div><input type="button" class="layer_btn lock" data-placement="top" data-original-title="unlock the layer"/>';
                        p = p + '<input type="button" class="layer_btn visible" data-placement="top" data-original-title="hide layer"/>';
                        p = p + ' <span class="layer_label" data-index="'+index+'"><label>R Square</label> </span>';
                        p = p + '<input type="button" class="layer_btn send_back" data-placement="top" data-original-title="send back"/>';
                        p = p + '<input type="button" class="layer_btn bring_front" data-placement="top" data-original-title="bring front" data-index="'+index+'"/>';
                        p = p + '<input type="button" class="layer_btn delete" data-placement="top" data-original-title="Delete"  data-index="'+index+'"/></div>';
                    e("#layer_canvas").append(p);
                } else if (t[index].name == "Square") {
                    var p = '<div><input type="button" class="layer_btn lock" data-placement="top" data-original-title="unlock the layer"/>';
                        p = p + '<input type="button" class="layer_btn visible" data-placement="top" data-original-title="hide layer"/>';
                        p = p + ' <span class="layer_label" data-index="'+index+'"><label>Square</label> </span>';
                        p = p + '<input type="button" class="layer_btn send_back" data-placement="top" data-original-title="send back"/>';
                        p = p + '<input type="button" class="layer_btn bring_front" data-placement="top" data-original-title="bring front" data-index="'+index+'"/>';
                        p = p + '<input type="button" class="layer_btn delete" data-placement="top" data-original-title="Delete"  data-index="'+index+'"/></div>';
                    e("#layer_canvas").append(p);
                } else if (t[index].name == "Polygon") {
                    var p = '<div><input type="button" class="layer_btn lock" data-placement="top" data-original-title="unlock the layer"/>';
                        p = p + '<input type="button" class="layer_btn visible" data-placement="top" data-original-title="hide layer"/>';
                        p = p + ' <span class="layer_label" data-index="'+index+'"><label>Polygon</label> </span>';
                        p = p + '<input type="button" class="layer_btn send_back" data-placement="top" data-original-title="send back"/>';
                        p = p + '<input type="button" class="layer_btn bring_front" data-placement="top" data-original-title="bring front" data-index="'+index+'"/>';
                        p = p + '<input type="button" class="layer_btn delete" data-placement="top" data-original-title="Delete"  data-index="'+index+'"/></div>';
                    e("#layer_canvas").append(p);
                } else if (t[index].name == "Star") {
                    var p = '<div><input type="button" class="layer_btn lock" data-placement="top" data-original-title="unlock the layer"/>';
                        p = p + '<input type="button" class="layer_btn visible" data-placement="top" data-original-title="hide layer"/>';
                        p = p + ' <span class="layer_label" data-index="'+index+'"><label>Star</label> </span>';
                        p = p + '<input type="button" class="layer_btn send_back" data-placement="top" data-original-title="send back"/>';
                        p = p + '<input type="button" class="layer_btn bring_front" data-placement="top" data-original-title="bring front" data-index="'+index+'"/>';
                        p = p + '<input type="button" class="layer_btn delete" data-placement="top" data-original-title="Delete"  data-index="'+index+'"/></div>';
                    e("#layer_canvas").append(p);
                } else if (t[index].name == "Triangle") {
                    var p = '<div><input type="button" class="layer_btn lock" data-placement="top" data-original-title="unlock the layer"/>';
                        p = p + '<input type="button" class="layer_btn visible" data-placement="top" data-original-title="hide layer"/>';
                        p = p + ' <span class="layer_label" data-index="'+index+'"><label>Triangle</label> </span>';
                        p = p + '<input type="button" class="layer_btn send_back" data-placement="top" data-original-title="send back"/>';
                        p = p + '<input type="button" class="layer_btn bring_front" data-placement="top" data-original-title="bring front"  data-index="'+index+'"/>';
                        p = p + '<input type="button" class="layer_btn delete" data-placement="top" data-original-title="Delete"  data-index="'+index+'"/></div>';
                    e("#layer_canvas").append(p);
                } else  {
                    var p = '<div><input type="button" class="layer_btn lock" data-placement="top" data-original-title="unlock the layer"/>';
                        p = p + '<input type="button" class="layer_btn visible" data-placement="top" data-original-title="hide layer"/>';
                        p = p + ' <span class="layer_label" data-index="'+index+'"><label>Art/Image</label> </span>';
                        p = p + '<input type="button" class="layer_btn send_back" data-placement="top" data-original-title="send back"/>';
                        p = p + '<input type="button" class="layer_btn bring_front" data-placement="top" data-original-title="bring front"  data-index="'+index+'"/>';
                        p = p + '<input type="button" class="layer_btn delete" data-placement="top" data-original-title="Delete"  data-index="'+index+'"/></div>';
                    e("#layer_canvas").append(p);
                }
            });
        }
    }
    function A(t, n) {
        e("#" + t).css("background-color", "#" + n);
        var r = w.getActiveObject();
        if (r != null) {
            if (t == "txt-color-selector" || t == "shape-bg-color-selector" || t == "clipart-bg-color-selector") r.set("fill", "#" + n);
            else if (t == "txt-bg-color-selector") r.set("backgroundColor", "#" + n);
            else if (t == "txt-outline-color-selector" || t == "shape-outline-color-selector") r.set("stroke", "#" + n);
            else console.log("unknow color selector :#" + t);
            w.renderAll()
        }
    }

    function O(t, n, r) {
        e("#" + t).css("background-color", "#" + n);
        var i = w.getActiveObject();
        if (i != null && (i.type == "path" || i.type == "path-group")) {
            {
                if (i.isSameColor && i.isSameColor() || !i.paths) {
                    i.set("fill", "#" + n)
                } else if (i.paths) {
                    if (g == "by-colors") {
                        r = e("#" + t).attr("data-index");
                        var s = r.split(",");
                        e.each(s, function(e, t) {
                            i.paths[t].setFill("#" + n)
                        })
                    } else i.paths[r].setFill("#" + n)
                }
            }
            w.renderAll()
        }
    }

    function M(t) {
        var n = e("#" + t);
        var r = n.data("index");
        var i = n.css("background-color");
        if (!i) i = "#0000ff";
        if (y == "custom") {
            n.qtip({
                content: "<div class='wpc-custom-svg-colors-container' data-id='" + t + "' data-index='" + r + "'>" + palette_tpl + "</div>",
                position: {
                    corner: {
                        target: "middleRight",
                        tooltip: "leftTop"
                    }
                },
                style: {
                    width: 200,
                    padding: 5,
                    background: "white",
                    color: "black",
                    border: {
                        width: 1,
                        radius: 1,
                        color: "#08AED6"
                    }
                },
                tip: "bottomLeft",
                show: "click",
                hide: {
                    when: {
                        event: "unfocus"
                    }
                }
            })
        } else {
            n.ColorPicker({
                color: i,
                onShow: function(t) {
                    e(t).fadeIn(500);
                    return false
                },
                onHide: function(t) {
                    e(t).fadeOut(500);
                    var n = w.getActiveObject();
                    if (n != null) {
                        Q()
                    }
                    return false
                },
                onChange: function(e, n, i) {
                    O(t, n, r)
                }
            })
        }
    }

    function _(e, t) {
        var n = w.getActiveObject();
        var r = jQuery.inArray(e.type.toLowerCase(), k);
        if (n != null && n.type == "image") {
            if (t) n.filters[r] = e;
            else n.filters[r] = false;
            n.applyFilters(w.renderAll.bind(w));
            Q()
        }
    }

    function D() {
        var t = new fabric.Text("Loading fonts...", {
            left: 200,
            top: 125,
            fontSize: 1,
            fontWeight: "bold",
            fontStyle: "italic",
            textDecoration: "underline",
            useNative: true,
            visible:true
        });
        //w.add(t);
        e("#font-family-selector > option").each(function() {
            t.set("fontFamily", e(this).val());
            w.renderAll()
        });
        //w.remove(t)
    }

    function P() {
        e("#new-text").val("");
        e("#underline-cb").removeAttr("checked");
        e("#bold-cb").removeAttr("checked");
        e("#italic-cb").removeAttr("checked");
        e("#font-family-selector").val(e("#font-family-selector option:first").val());
        e("#font-family-selector").val(e("#font-family-selector option:first").val());
        e("#o-thickness-slider").val(e("#o-thickness-slider option:first").val());
        e("#opacity-slider").val(1)
    }

    function H(e, t, n) {
        var r = w.getActiveObject();
        if (r.filters[e]) {
            r.filters[e][t] = n;
            r.applyFilters(w.renderAll.bind(w))
        }
    }

    function B(t) {
        var n = e("#o-thickness-slider").val();
        var r = "normal";
        var i = "";
        var s = "";
        var o = e("#txt-color-selector").css("background-color");
        var u = e("#font-family-selector").val();
        var a = parseInt(e("#font-size-slider").val());
        var f = e("#opacity-slider").val();
        var l = e("#txt-outline-color-selector").css("background-color");
        var c = e("#txt-bg-color-selector").css("background-color");
        var h = e("#bold-cb").is(":checked");
        var p = e("#underline-cb").is(":checked");
        var d = e("#italic-cb").is(":checked");
        if (h) r = "bold";
        if (p) i = "underline";
        if (d) s = "italic";
        var v = new fabric.Text(t, {
            left: 30,
            top: 70,
            fontFamily: u,
            fontSize: a,
            fontWeight: r,
            fontStyle: s,
            textDecoration: i,
            selectable: true,
            fill: o,
            visible: true
        });
        if (n > 0) {
            v.set("stroke", l);
            v.set("strokeWidth", parseInt(n))
        }
        lt(v);
        return v
    }

    function j(t) {
        var n = B(t);
        w.add(n);
        w.centerObjectH(n);
        w.centerObjectV(n);
        w.renderAll();
        n.setCoords();
        e("#new-text").val("");
        Q()
    }

    function F(e, t) {
        var n = fabric.util.object.clone(e);
        n.set("top", n.top + 5);
        n.set("left", n.left + 5);
        lt(n);
        w.add(n);
        if (t) {
            w.renderAll();
            Q()
        }
    }

    function I(t, n, r) {
        var i = t.length;
        var s;
        var o = e("#curved-txt-radius-slider").val();
        var u = e("#curved-txt-spacing-slider").val();
        var a = 0;
        var f = 0;
        var l = 0;
        var c = w.getWidth() / 2;
        var h = w.getHeight() - 30;
        l = u / 2 * (i - 1);
        var p = false;
        var d = 1;
        if (p) d = -1;
        var v = [];
        for (var m = 0; m < i; m++) {
            s = t[m];
            var g = B(s);
            a = m * parseInt(u, 10) - l;
            f = a * (Math.PI / 180);
            if (p) {
                a = -m * parseInt(u, 10) + l;
                f = a * (Math.PI / 180)
            }
            var y = (c + -Math.cos(f) * o) * d;
            var b = (h + Math.sin(f) * o) * d;
            g.set("top", y);
            g.set("left", b);
            g.setAngle(a);
            v.push(g)
        }
        var E = new fabric.Group(v, {
            left: 150,
            top: 100,
            visible: true
        });
        if (n != null) w.setActiveObject(E);
        lt(E);
        E["originalText"] = t;
        E["radius"] = o;
        E["spacing"] = u;
        w.add(E);
        if (n == null) E.center();
        else {
            E.set("left", r);
            E.set("top", n)
        }
        Q();
        w.renderAll();
        E.setCoords()
    }

    function q(e) {
        var t = e.get("left");
        var n = e.get("top");
        w.remove(e);
        I(e.originalText, n, t)
    }

    function U(e) {
        var t = w.getWidth();
        var n = w.getHeight();
        if (o && u && o > 0 && u > 0 && h == "rect") {
            t = o;
            n = u
        } else if (a && a > 0 && h == "arc") {
            t = a;
            n = a
        }
        var r = rt(e, t, n);
        var i = t / r[0];
        var s = n / r[1];
        if (i > s) e.scaleToWidth(r[0]);
        else e.scaleToHeight(r[1])
    }

    function z(e, t) {
        var n = e.split(".").pop();
        if (typeof t == "undefined") t = 0;
        if (n == "svg") {
            fabric.loadSVGFromURL(e, function(e, n) {
                var r = fabric.util.groupSVGElements(e, n);
                U(r);
                lt(r);
                r.set("price", t);
                w.add(r).centerObject(r).calcOffset().renderAll();
                r.setCoords();
                Q()
            })
        } else {
            fabric.Image.fromURL(e, function(e) {
                U(e);
                lt(e);
                e.set("price", t);
                w.add(e.set({
                    left: 100,
                    top: 100,
                    angle: 0,
                    visible: true
                })).centerObject(e).renderAll();
                e.setCoords();
                Q()
            })
        }
    }

    function W(e) {
        var t = window.location.search.substring(1);
        var n = t.split("&");
        for (var r = 0; r < n.length; r++) {
            var i = n[r].split("=");
            if (i[0] == e) {
                return i[1]
            }
        }
    }

    function X(e) {
        V(e, false)
    }

    function V(t, n) {
        N = t;
        n = typeof n !== "undefined" ? n : true;
        var r = e("#wpc-parts-bar > span:eq(" + t + ")").attr("data-id");
        var i = e("#wpc-parts-bar > span:eq(" + t + ")").attr("data-url");
        w.clear();
        w.loadFromJSON(x[r][C[r]], function() {
            it(N, function() {
                var t = w.toDataURL({
                    format: "png",
                    multiplier: b,
                    quality: 1
                });
                if (n) {
                    var s = "";
                    if (i) s = "<div style='background-image:url(" + i + ");'><img src='" + t + "'></div>";
                    else s = "<div><img src='" + t + "'></div>";
                    e("#wpc-modal .modal-body").append(s)
                } else {
                    var o = e.parseJSON(x[r][C[r]]);
                    var u = [];
                    if (m) {
                        var a = o.objects;
                        e.each(a, function(e, t) {
                            w.clear();
                            var n = o;
                            n.objects = [t];
                            var r = JSON.stringify(n);
                            w.loadFromJSON(r, function() {
                                w.renderAll.bind(w);
                                var e = w.toDataURL({
                                    format: "png",
                                    multiplier: b,
                                    quality: 1
                                });
                                u.push(e)
                            })
                        })
                    }
                    T[r] = {
                        json: x[r][C[r]],
                        image: t,
                        original_part_img: i,
                        layers: u
                    }
                }
            })
        })
    }

    function $(t) {
        if (typeof t == "object") {
            var n = true;
            e.each(t, function(t, r) {
                e.each(r, function(e, r) {
                    if (e == "json") {
                        x[t] = [];
                        C[t] = 0;
                        var i = r;
                        x[t].push(i);
                        if (n) {
                            N = 0;
                            w.loadFromJSON(i, function() {
                                w.renderAll.bind(w)
                            });
                            w.calcOffset();
                            ut()
                        }
                    }
                });
                n = false
            })
        }
    }

    function J() {
        var t = e("#wpc-parts-bar > span:eq(0)").attr("data-id");
        w.clear();
        w.loadFromJSON(x[t][C[t]], function() {
            w.renderAll.bind(w)
        })
    }

    function K() {
        var t = e("#wpc-parts-bar > span:eq(" + N + ")").attr("data-id");
        if (x[t].length == 1 || C[t] == 0) e("#undo-btn").addClass("disabled");
        else e("#undo-btn").removeClass("disabled");
        if (x[t].length > 0 && C[t] < x[t].length - 1) e("#redo-btn").removeClass("disabled");
        else e("#redo-btn").addClass("disabled")
    }

    function Q() {
        var t = e("#wpc-parts-bar > span:eq(" + N + ")").attr("data-id");
        for (i = C[t]; i <= x[t].length - 2; i++) {
            x[t].pop()
        }
        C[t] ++;
        var n = JSON.stringify(w.toJSON(["lockMovementX", "lockMovementY", "lockRotation", "lockScalingX", "lockScalingY", "price", "lockDeletion", "originalText", "radius", "spacing"]));
        x[t].push(n);
        G();
        K()
    }

    function G() {
        var t = e("#wpc-parts-bar > span").length;
        var n = {};
        e.each(e("#wpc-parts-bar > span"), function(r, i) {
            var s = e(this).attr("data-id");
            if (x[s]) n[s] = {
                json: x[s][C[s]]
            };
            if (e(this).index() == t - 1) {
                var o = global_variation_id;
                e.post(ajax_object.ajax_url, {
                    action: "get_design_price",
                    variation_id: o,
                    serialized_parts: n
                }, function(t) {
                    e("#total_order").text(t.price);
                    e("#wpc-qty").attr("uprice", t.price);
                    e("#wpc-qty").trigger("change")
                }, "json")
            }
        })
    }

    function Y(t, n, r, i) {
        var s = e.parseJSON(t);
        if (s.success) {
            if (e("#uploads-accordion .AccordionPanelContent").text() == "Empty") e("#uploads-accordion .AccordionPanelContent").text("");
            e("#uploads-accordion .AccordionPanelContent").append(s.message);
            var o = e("#uploads-accordion .AccordionPanelContent img").length;
            e("#uploads-accordion .AccordionPanelTab").text("Uploads (" + o + ")")
        } else alert(s.message);
        e("#userfile").val("")
    }

    function Z(t, n, r, i) {
        var s = e.parseJSON(t);
        if (s.success) {
            e("#wpc-uploaded-file").html(s.message)
        } else alert(s.message);
        e("#user-custom-design").val("")
    }

    function tt(e, t) {
        var n = 0;
        for (var r = 0, i = t.length; r < i; r++) {
            if (t[r] == e) return r;
            n++
        }
        return -1
    }

    function nt(e) {
        if (typeof e !== "number") {
            return ""
        }
        if (e >= 1e9) {
            return (e / 1e9).toFixed(2) + " GB"
        }
        if (e >= 1e6) {
            return (e / 1e6).toFixed(2) + " MB"
        }
        return (e / 1e3).toFixed(2) + " KB"
    }

    function rt(e, t, n) {
        var r = e.width;
        var i = e.height;
        var s = r / i;
        r = t;
        i = t / s;
        if (i > n) {
            i = n;
            r = n * s
        }
        return [r, i]
    }

    function it(t, n) {
        var i = e("#wpc-parts-bar > span:eq(" + t + ")");
        var o = i.data("bg");
        if (o == "") o = null;
        var u = i.data("ov");
        if (u == "") u = null;
        var a = new Image;
        a.onload = function() {
            var e = rt(a, r, s);
            w.setBackgroundImage(a.src, w.renderAll.bind(w), {
                left: r / 2,
                top: s / 2,
                originX: "center",
                originY: "center",
                width: e[0],
                height: e[1]
            })
        };
        if (o != null) a.src = o;
        var f = new Image;
        f.onload = function() {
            var e = rt(f, r, s);
            w.setOverlayImage(f.src, w.renderAll.bind(w), {
                left: r / 2,
                top: s / 2,
                originX: "center",
                originY: "center",
                width: e[0],
                height: e[1]
            })
        };
        if (u != null) f.src = u;
        if (e.isFunction(n)) setTimeout(function() {
            n(t)
        }, 200)
    }

    function st(t, n, r) {
        e.blockUI({
            message: loading_msg
        });
        var i = e("#wpc-parts-bar > span").length;
        var s = 0;
        var o = setInterval(function() {
            if (e.isFunction(n)) n(s);
            if (s == i - 1) {
                window.clearInterval(o);
                if (e.isFunction(r)) {
                    setTimeout(function() {
                        r()
                    }, t)
                } else e.unblockUI()
            } else s++
        }, t)
    }

    function ot(t) {
        e("#wpc-parts-bar > span:eq(" + t + ")").click()
    }

    function ut() {
        var t = e("#wpc-parts-bar > span").first().attr("data-url");
        var n = "url('" + t + "') no-repeat center center";
        e("#wpc-editor-container").css("background", n)
    }

    function ft() {
        setTimeout(function() {
            e("#img-cliparts-container").perfectScrollbar("update")
        }, 100)
    }

    function lt(e) {
        e.toObject = function(e) {
            return function() {
                return fabric.util.object.extend(e.call(this), {
                    lockMovementX: this.lockMovementX,
                    lockMovementY: this.lockMovementY,
                    lockScalingX: this.lockScalingX,
                    lockScalingY: this.lockScalingY,
                    lockDeletion: this.lockDeletion,
                    price: this.price,
                    originalText: this.originalText,
                    radius: this.radius,
                    spacing: this.spacing
                })
            }
        }(e.toObject)
    }
    var t = new Spry.Widget.Accordion("img-cliparts-accordion", {
        useFixedPanelHeights: false,
        defaultPanel: -1
    });
    e("#wpc-top-bar > span").click(function() {
        e("#wpc-top-bar > span").removeClass("selected");
        e(this).addClass("selected");
        e("#wpc-tools-bar > div").hide();
        var t = e(this).attr("data-id");
        e(t).show()
    });
    e(".noUiSlider").each(function() {
        var t = e(this).attr("data-min");
        if (t == null) t = 0;
        var n = e(this).attr("data-max");
        if (n == null) n = 1;
        var r = e(this).attr("data-step");
        if (r == null) r = .1;
        var i = e(this).attr("data-start");
        if (i == null) r = 1;
        e(this).wpcnoUiSlider({
            start: [parseInt(i)],
            step: parseFloat(r),
            range: {
                min: parseInt(t),
                max: parseInt(n)
            }
        })
    });
    var r = e("#wpc-editor-container").width();
    var s = e("#wpc-editor-container").height();
    var o = e("#wpc-editor-container").data("clip_w");
    var u = e("#wpc-editor-container").data("clip_h");
    var a = e("#wpc-editor-container").data("clip_r");
    var f = e("#wpc-editor-container").data("clip_rr");
    var l = e("#wpc-editor-container").data("clip_x");
    var c = e("#wpc-editor-container").data("clip_y");
    var h = e("#wpc-editor-container").data("clip_type");
    var p = e("#wpc-editor-container").data("clip_border");
    var d = e("#wpc-editor-container").data("output_w");
    var v = e("#wpc-editor-container").data("output_delay");
    var m = e("#wpc-editor-container").data("print_layers");
    var g = e("#wpc-editor-container").data("svg_colorization");
    var y = e("#wpc-editor-container").data("palette_type");
    if (!d) d = r;
    var b = d / r;
    var w = new fabric.Canvas("wpc-editor", {
        width: r,
        height: s
    });
    w.setWidth(r);
    w.setHeight(s);
    w.backgroundImageStretch = false;
    if (o && u && o > 0 && u > 0 && h == "rect") {
        var E = (r - o) / 2;
        if (l) E = l;
        var S = (s - u) / 2;
        if (c) S = c;
        w.clipTo = function(e) {
            if (h == "rect" || h == "") {
                if (f > 0) n(e, E, S, o, u, f, "", p);
                else {
                    e.rect(E, S, o, u);
                    if (p) {
                        e.strokeStyle = p;
                        e.stroke()
                    }
                }
            }
        }
    } else if (a && a > 0 && h == "arc") {
        var E = r / 2;
        if (l) E = l;
        var S = s - u / 2;
        if (c) S = c;
        w.clipTo = function(e) {
            e.arc(E, S, a, 0, 2 * Math.PI);
            if (p) {
                e.strokeStyle = p;
                e.stroke()
            }
        }
    }
    w.renderAll();
    var x = [];
    var T = {};
    var N = -1;
    var C = [];
    var k = ["grayscale", "invert", "remove-white", "sepia", "sepia2", "brightness", "noise", "gradient-transparency", "pixelate", "blur", "convolute"];
    L();
    e('[id$="color-selector"]').each(function() {
        var t = e(this).attr("id");
        var n = e(this).css("background-color");
        if (!n) n = "#0000ff";
        if (y == "custom") {
            e("#" + t).qtip({
                content: "<div class='wpc-custom-colors-container' data-id='" + t + "'>" + palette_tpl + "</div>",
                position: {
                    corner: {
                        target: "middleRight",
                        tooltip: "leftTop"
                    }
                },
                style: {
                    width: 200,
                    padding: 5,
                    background: "white",
                    color: "black",
                    border: {
                        width: 1,
                        radius: 1,
                        color: "#08AED6"
                    }
                },
                tip: "bottomLeft",
                show: "click",
                hide: {
                    when: {
                        event: "unfocus"
                    }
                }
            })
        } else {
            e("#" + t).ColorPicker({
                color: n,
                onShow: function(t) {
                    e(t).fadeIn(500);
                    return false
                },
                onHide: function(t) {
                    e(t).fadeOut(500);
                    var n = w.getActiveObject();
                    if (n != null) {
                        Q()
                    }
                    return false
                },
                onChange: function(e, n, r) {
                    A(t, n)
                }
            })
        }
    });
    e(document).on("click", ".wpc-custom-colors-container span", function(t) {
        var n = e(this).parent().data("id");
        var r = e(this).data("color");
        A(n, r)
    });
    e(document).on("click", ".wpc-custom-svg-colors-container span", function(t) {
        var n = e(this).parent().data("id");
        var r = e(this).parent().data("index");
        var i = e(this).data("color");
        O(n, i, r)
    });
    e("#grayscale").change(function() {
        _(new fabric.Image.filters.Grayscale, e(this).is(":checked"))
    });
    e("#invert").change(function() {
        _(new fabric.Image.filters.Invert, e(this).is(":checked"))
    });
    e("#sepia").change(function() {
        _(new fabric.Image.filters.Sepia, e(this).is(":checked"))
    });
    e("#sepia2").change(function() {
        _(new fabric.Image.filters.Sepia2, e(this).is(":checked"))
    });
    e("#blur").change(function() {
        if (e(this).is(":checked")) e("#sharpen, #emboss").removeAttr("checked");
        _(new fabric.Image.filters.Convolute({
            matrix: [1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9]
        }), e(this).is(":checked"))
    });
    e("#sharpen").change(function() {
        if (e(this).is(":checked")) e("#blur, #emboss").removeAttr("checked");
        _(new fabric.Image.filters.Convolute({
            matrix: [0, -1, 0, -1, 5, -1, 0, -1, 0]
        }), e(this).is(":checked"))
    });
    e("#emboss").change(function() {
        if (e(this).is(":checked")) e("#sharpen, #blur").removeAttr("checked");
        _(new fabric.Image.filters.Convolute({
            matrix: [1, 1, 1, 1, .7, -1, -1, -1, -1]
        }), e(this).is(":checked"))
    });
    e("#new-text").keyup(function() {
        var t = w.getActiveObject();
        var n = e("#new-text").val();
        if (t != null && t.type == "text") {
            t.set("text", n);
            Q();
            w.renderAll()
        } else if (t != null && t.type == "group") {
            var r = t.get("left");
            var i = t.get("top");
            w.remove(t);
            I(n, i, r)
        }
    });
    e("#shape-thickness-slider").change(function() {
        var t = w.getActiveObject();
        if (t != null) {
            var n = ["rect", "circle", "triangle", "polygon"];
            if (jQuery.inArray(t.type, n) >= 0) {
                var r = e("#shape-outline-color-selector").css("background-color");
                var i = e("#shape-thickness-slider").val();
                if (t != null) {
                    if (i > 0) {
                        t.set("strokeWidth", parseInt(i));
                        t.set("stroke", r)
                    } else t.set("stroke", false)
                }
                w.renderAll()
            }
        }
    });
    e(".noUiSlider").each(function() {
        var t = e(this).attr("data-min");
        if (t == null) t = 0;
        var n = e(this).attr("data-max");
        if (n == null) n = 1;
        var r = e(this).attr("data-step");
        if (r == null) r = .1;
        var i = e(this).attr("data-start");
        if (i == null) i = 1;
        var s = [i]
    });
    e("#add-text-btn").click(function() {
        var t = e("#new-text").val();
        var n = e("#is-curved").is(":checked");
        if (t.length == 0) alert("Please enter the text to add.");
        else if (!n) j(t);
        else {
            I(t)
        }
    });
    e("#delete_btn").click(function() {
        var e = w.getActiveObject();
        var t = w.getActiveGroup();
        var n;
        if (e != null) {
            if (e["lockDeletion"]) {
                alert(deletion_error_msg)
            } else {
                n = confirm("Do you really want to delete the selected items?");
                if (n) {
                    w.remove(e);
                    w.calcOffset();
                    w.renderAll();
                    Q()
                }
            }
        } else if (t != null) {
            if (t["lockDeletion"]) {
                alert(deletion_error_msg)
            } else {
                n = confirm("Do you really want to delete the selected items?");
                if (n) {
                    t.forEachObject(function(e) {
                        w.remove(e)
                    });
                    w.discardActiveGroup();
                    w.calcOffset();
                    w.renderAll();
                    Q()
                }
            }
        }
    });
    e("#copy_paste_btn").click(function() {
        var t = w.getActiveObject();
        var n = w.getActiveGroup();
        if (n != null) {
            var r = new fabric.Group;
            w.discardActiveGroup();
            w.renderAll();
            var i = w.getObjects();
            e.each(i, function(e, t) {
                if (n.contains(t)) {
                    F(t, false)
                }
            });
            w.renderAll();
            Q()
        } else if (t != null) {
            w.discardActiveObject();
            F(t, true);
            Q()
        }
    });
    e("#clear_all_btn").click(function() {
        var e = confirm("Do you really want to delete all items in the design area?");
        if (e) {
            w.clear();
            Q()
        }
    });
    var R = 1;
    e("#zoom-in-btn").click(function() {
        R += .2;
        w.setZoom(R)
    });
    e("#zoom-out-btn").click(function() {
        R -= .2;
        w.setZoom(R)
    });
    e("#grid-btn").click(function() {
        e("#wpc-editor-container").toggleClass("wpc-canvas-grid")
    });
    e("#bring_to_front_btn").click(function() {
        var e = w.getActiveObject();
        var t = w.getActiveGroup();
        if (e != null) {
            w.bringForward(e);
            w.renderAll();
            Q()
        } else if (t != null) {
            t.forEachObject(function(e) {
                w.bringForward(e)
            });
            w.discardActiveGroup();
            w.renderAll();
            Q()
        }
    });

    e('.layers_implement').on('click',function() {
            var $thatInput = $(this).siblings("input").val();
            console.log($thatInput);
    });
    e("#send_to_back_btn").click(function() {
        var e = w.getActiveObject();
        var t = w.getActiveGroup();
        if (e != null) {
            w.sendBackwards(e);
            w.renderAll();
            Q()
        } else if (t != null) {
            t.forEachObject(function(e) {
                w.sendBackwards(e)
            });
            w.discardActiveGroup();
            w.renderAll();
            Q()
        }
    });

   /* e(".visible").click(function() {
            if(e(this).hasClass('visible'))
            {
                e(this).addClass('invisible').removeClass('visible')
            }
            else{
                e(this).addClass('visible').removeClass('invisible')
            }
            return false;
    }); */

    e('#layer_canvas').on('click','.visible',function() {
        var i = w.getActiveObject();
        var t = w.getActiveGroup();
        if (i != null) {
            e(".visible").toggleClass( "invisible", 1000 );
            if (i.get("visible") == true)
                {
                    i.set("visible", false);
                    

                }
            else{
                    i.set("visible", true);
            }
            w.renderAll();
            Q()
        } else if (t != null) {
            t.forEachObject(function(i) {
                if (i.get("visible") == true) i.set("visible", false);
                else i.set("visible", true);
            });
            w.discardActiveGroup();
            w.renderAll();
            Q()
        }
        return false;
    });
    e('#layer_canvas').on('click','.bring_front',function() {
        var t1 = w.getObjects();
        console.log(e(this).data('index'));
        w.setActiveObject(w.item(e(this).data('index')));
        var j = w.getActiveObject();
        var t = w.getActiveGroup();
        if (j != null) {
            w.bringForward(j);
            w.renderAll();
            Q()
        } else if (t != null) {
            t.forEachObject(function(j) {
                w.bringForward(j)
            });
            w.discardActiveGroup();
            w.renderAll();
            Q()
        }
        return false;
    });

    e('#layer_canvas').on('click','.send_back',function() {

        var e = w.getActiveObject();
        var t = w.getActiveGroup();
        if (e != null) {
            w.sendBackwards(e);
            w.renderAll();
            Q()
        } else if (t != null) {
            t.forEachObject(function(e) {
                w.sendBackwards(e)
            });
            w.discardActiveGroup();
            w.renderAll();
            Q()
        }
        return false;
    });
    e('#layer_canvas').on('click','.delete',function() {
        w.setActiveObject(w.item(e(this).data('index')));
        var i = w.getActiveObject();
        var t = w.getActiveGroup();
        var n;
        if (i != null) {
            if (i["lockDeletion"]) {
                alert(deletion_error_msg)
            } else {
                n = confirm("Do you really want to delete the selected items?");
                if (n) {
                    w.remove(i);
                    w.calcOffset();
                    w.renderAll();
                    AB();
                    Q();

                }
            }
        }
        return false;
    });
    e('#layer_canvas').on('click','.lock',function() {
        var i = w.getActiveObject();
        var t = w.getActiveGroup();
        if (i != null) {
            e(".lock").toggleClass( "unlock", 1000 );
            if (i.get("selectable") == true)
                {
                    i.set("selectable", false);
                    i.set("lockMovementX", true);
                    i.set("lockMovementY", true);
                    i.set("hasControls", false);
                }
            else{
                i.set("selectable", true);
                i.set("lockMovementX", false);
                i.set("lockMovementY", false);
                i.set("hasControls", true);

            }
            w.renderAll();
            Q()
        } else if (t != null) {
            t.forEachObject(function(i) {
                if (i.get("selectable") == true){
                    i.set("selectable", false);
                    i.set("lockMovementX", true);
                    i.set("lockMovementY", true);
                    i.set("hasControls", false);

                }
                else{
                    i.set("selectable", true);
                    i.set("lockMovementX", false);
                    i.set("lockMovementY", false);
                    i.set("hasControls", true);

                }
            });
            w.discardActiveGroup();
            w.renderAll();
            Q()
        }
        return false;
    });

    e("#align_h_btn").click(function() {
        var e = w.getActiveObject();
        var t = w.getActiveGroup();
        if (e != null) {
            w.centerObjectH(e);
            w.renderAll();
            e.setCoords();
            Q()
        } else if (t != null) {
            w.centerObjectH(t);
            w.renderAll();
            t.setCoords();
            Q()
        }
    });
    e("#align_v_btn").click(function() {
        var e = w.getActiveObject();
        var t = w.getActiveGroup();
        if (e != null) {
            w.centerObjectV(e);
            w.renderAll();
            e.setCoords();
            Q()
        } else if (t != null) {
            w.centerObjectV(t);
            w.renderAll();
            t.setCoords();
            Q()
        }
    });
    e("#flip_h_btn").click(function() {
        var e = w.getActiveObject();
        var t = w.getActiveGroup();
        if (e != null) {
            if (e.get("flipX") == true) e.set("flipX", false);
            else e.set("flipX", true);
            w.renderAll();
            Q()
        } else if (t != null) {
            if (t.get("flipX") == true) t.set("flipX", false);
            else t.set("flipX", true);
            w.renderAll();
            Q()
        }
    });
    e("#flip_v_btn").click(function() {
        var e = w.getActiveObject();
        var t = w.getActiveGroup();
        if (e != null) {
            if (e.get("flipY") == true) e.set("flipY", false);
            else e.set("flipY", true);
            w.renderAll();
            Q()
        } else if (t != null) {
            if (t.get("flipY") == true) t.set("flipY", false);
            else t.set("flipY", true);
            w.renderAll();
            Q()
        }
    });
    e.fn.add_shape = function(t) {
        var n = e("#shape-opacity-slider").val();
        var r = "background-color:#ed8a46;";
        r += "opacity:" + n + ";";
        r += "left:0px;top:0px;";
        var i = "<div class='draggable resizable " + t + "' style='" + r + "'></div>";
        this.append(i);
        e.load_design_area_features();
        e.reset_palette()
    };
    e("#square-btn").click(function() {
        var t = e("#shape-bg-color-selector").css("background-color");
        var n = e("#shape-outline-color-selector").css("background-color");
        var r = e("#shape-opacity-slider").val();
        var i = new fabric.Rect({
            left: 100,
            top: 50,
            fill: t,
            opacity: r,
            width: 50,
            height: 50,
            name: "Square",
            visible:true,
        });
        var s = e("#shape-thickness-slider").val();
        if (s > 0) {
            i.set("strokeWidth", parseInt(s));
            i.set("stroke", n)
        }
        lt(i);
        w.add(i);
        w.centerObjectH(i);
        w.centerObjectV(i);
        w.renderAll();
        i.setCoords();
        Q()
    });
    e("#r-square-btn").click(function() {
        var t = e("#shape-bg-color-selector").css("background-color");
        var n = e("#shape-outline-color-selector").css("background-color");
        var r = e("#shape-opacity-slider").val();
        var i = new fabric.Rect({
            left: 100,
            top: 50,
            fill: t,
            opacity: r,
            width: 50,
            height: 50,
            rx: 10,
            ry: 10,
            selectable: true,
            name: "R Square",
            visible: true
        });
        var s = e("#shape-thickness-slider").val();
        if (s > 0) {
            i.set("strokeWidth", parseInt(s));
            i.set("stroke", n)
        }
        lt(i);
        w.add(i);
        w.centerObjectH(i);
        w.centerObjectV(i);
        w.renderAll();
        i.setCoords();
        Q()
    });
    e("#circle-btn").click(function() {
        var t = e("#shape-bg-color-selector").css("background-color");
        var n = e("#shape-outline-color-selector").css("background-color");
        var r = e("#shape-opacity-slider").val();
        var i = new fabric.Circle({
            left: 100,
            top: 50,
            fill: t,
            opacity: r,
            radius: 25,
            selectable: true,
            name: "Circle",
            visible: true
        });
        var s = e("#shape-thickness-slider").val();
        if (s > 0) {
            i.set("strokeWidth", parseInt(s));
            i.set("stroke", n)
        }
        lt(i);
        w.add(i);
        w.centerObjectH(i);
        w.centerObjectV(i);
        w.renderAll();
        i.setCoords();
        Q()
    });
    e("#triangle-btn").click(function() {
        var t = e("#shape-bg-color-selector").css("background-color");
        var n = e("#shape-outline-color-selector").css("background-color");
        var r = e("#shape-opacity-slider").val();
        var i = new fabric.Triangle({
            left: 100,
            top: 50,
            fill: t,
            opacity: r,
            width: 50,
            height: 50,
            selectable: true,
            name: "Triangle",
            visible: true
        });
        var s = e("#shape-thickness-slider").val();
        if (s > 0) {
            i.set("strokeWidth", parseInt(s));
            i.set("stroke", n)
        }
        lt(i);
        w.add(i);
        w.centerObjectH(i);
        w.centerObjectV(i);
        w.renderAll();
        i.setCoords();
        Q()
    });
    e("#polygon-btn").click(function() {
        var t = e("#shape-bg-color-selector").css("background-color");
        var n = e("#shape-outline-color-selector").css("background-color");
        var r = e("#shape-opacity-slider").val();
        var i = e("#polygon-nb-points").val();
        var s = [];
        if (i == 5) {
            s = [{
                x: 0,
                y: 50
            }, {
                x: 45,
                y: 80
            }, {
                x: 85,
                y: 50
            }, {
                x: 70,
                y: 0
            }, {
                x: 17,
                y: 0
            }]
        } else if (i == 6) {
            s = [{
                x: 45,
                y: 90
            }, {
                x: 90,
                y: 70
            }, {
                x: 90,
                y: 20
            }, {
                x: 45,
                y: 0
            }, {
                x: 0,
                y: 20
            }, {
                x: 0,
                y: 70
            }]
        } else if (i == 7) {
            s = [{
                x: 26,
                y: 90
            }, {
                x: 65,
                y: 90
            }, {
                x: 88,
                y: 57
            }, {
                x: 81,
                y: 18
            }, {
                x: 45,
                y: 0
            }, {
                x: 12,
                y: 18
            }, {
                x: 0,
                y: 58
            }]
        } else if (i == 8) {
            s = [{
                x: 28,
                y: 90
            }, {
                x: 63,
                y: 90
            }, {
                x: 90,
                y: 63
            }, {
                x: 90,
                y: 27
            }, {
                x: 63,
                y: 0
            }, {
                x: 28,
                y: 0
            }, {
                x: 0,
                y: 27
            }, {
                x: 0,
                y: 63
            }]
        } else if (i == 9) {
            s = [{
                x: 45,
                y: 90
            }, {
                x: 75,
                y: 80
            }, {
                x: 90,
                y: 52
            }, {
                x: 85,
                y: 20
            }, {
                x: 60,
                y: 0
            }, {
                x: 30,
                y: 0
            }, {
                x: 8,
                y: 20
            }, {
                x: 0,
                y: 53
            }, {
                x: 17,
                y: 78
            }]
        } else if (i == 10) {
            s = [{
                x: 35,
                y: 90
            }, {
                x: 63,
                y: 90
            }, {
                x: 86,
                y: 74
            }, {
                x: 95,
                y: 47
            }, {
                x: 86,
                y: 19
            }, {
                x: 63,
                y: 0
            }, {
                x: 35,
                y: 0
            }, {
                x: 11,
                y: 19
            }, {
                x: 0,
                y: 45
            }, {
                x: 11,
                y: 72
            }]
        }
        var o = s.map(function(e) {
            return fabric.util.object.clone(e)
        });
        var u = new fabric.Polygon(o, {
            left: 100,
            top: 50,
            fill: t,
            opacity: r,
            selectable: true,
            name: "Polygon",
            visible: true
        });
        var a = e("#shape-thickness-slider").val();
        if (a > 0) {
            u.set("strokeWidth", parseInt(a));
            u.set("stroke", n)
        }
        lt(u);
        w.add(u);
        w.centerObjectH(u);
        w.centerObjectV(u);
        w.renderAll();
        u.setCoords();
        Q()
    });
    e("#star-btn").click(function() {
        var t = e("#shape-bg-color-selector").css("background-color");
        var n = e("#shape-outline-color-selector").css("background-color");
        var r = e("#shape-opacity-slider").val();
        var i = e("#star-nb-points").val();
        var s = [];
        if (i == 5) {
            s = [{
                x: 46,
                y: 90
            }, {
                x: 58,
                y: 56
            }, {
                x: 93,
                y: 55
            }, {
                x: 65,
                y: 35
            }, {
                x: 77,
                y: 0
            }, {
                x: 48,
                y: 22
            }, {
                x: 19,
                y: 0
            }, {
                x: 30,
                y: 35
            }, {
                x: 0,
                y: 56
            }, {
                x: 37,
                y: 56
            }]
        } else if (i == 6) {
            s = [{
                x: 40,
                y: 90
            }, {
                x: 54,
                y: 68
            }, {
                x: 79,
                y: 68
            }, {
                x: 66,
                y: 45
            }, {
                x: 79,
                y: 23
            }, {
                x: 53,
                y: 23
            }, {
                x: 40,
                y: 0
            }, {
                x: 26,
                y: 23
            }, {
                x: 0,
                y: 23
            }, {
                x: 14,
                y: 45
            }, {
                x: 0,
                y: 68
            }, {
                x: 26,
                y: 68
            }]
        } else if (i == 7) {
            s = [{
                x: 49,
                y: 90
            }, {
                x: 57,
                y: 60
            }, {
                x: 87,
                y: 74
            }, {
                x: 64,
                y: 47
            }, {
                x: 91,
                y: 34
            }, {
                x: 64,
                y: 34
            }, {
                x: 71,
                y: 0
            }, {
                x: 47,
                y: 26
            }, {
                x: 25,
                y: 0
            }, {
                x: 31,
                y: 32
            }, {
                x: 0,
                y: 32
            }, {
                x: 31,
                y: 47
            }, {
                x: 7,
                y: 74
            }, {
                x: 39,
                y: 60
            }]
        } else if (i == 8) {
            s = [{
                x: 46,
                y: 90
            }, {
                x: 52,
                y: 63
            }, {
                x: 77,
                y: 78
            }, {
                x: 61,
                y: 53
            }, {
                x: 89,
                y: 46
            }, {
                x: 61,
                y: 40
            }, {
                x: 77,
                y: 14
            }, {
                x: 52,
                y: 30
            }, {
                x: 46,
                y: 0
            }, {
                x: 37,
                y: 30
            }, {
                x: 14,
                y: 14
            }, {
                x: 27,
                y: 39
            }, {
                x: 0,
                y: 46
            }, {
                x: 27,
                y: 53
            }, {
                x: 13,
                y: 77
            }, {
                x: 37,
                y: 62
            }]
        } else if (i == 9) {
            s = [{
                x: 45,
                y: 90
            }, {
                x: 56,
                y: 73
            }, {
                x: 74,
                y: 69
            }, {
                x: 71,
                y: 59
            }, {
                x: 88,
                y: 52
            }, {
                x: 74,
                y: 39
            }, {
                x: 84,
                y: 21
            }, {
                x: 65,
                y: 21
            }, {
                x: 61,
                y: 0
            }, {
                x: 45,
                y: 14
            }, {
                x: 30,
                y: 0
            }, {
                x: 26,
                y: 21
            }, {
                x: 21,
                y: 6
            }, {
                x: 16,
                y: 39
            }, {
                x: 0,
                y: 51
            }, {
                x: 18,
                y: 59
            }, {
                x: 16,
                y: 79
            }, {
                x: 34,
                y: 73
            }]
        } else if (i == 10) {
            s = [{
                x: 35,
                y: 90
            }, {
                x: 50,
                y: 81
            }, {
                x: 63,
                y: 90
            }, {
                x: 69,
                y: 73
            }, {
                x: 88,
                y: 73
            }, {
                x: 82,
                y: 56
            }, {
                x: 96,
                y: 46
            }, {
                x: 82,
                y: 36
            }, {
                x: 87,
                y: 18
            }, {
                x: 70,
                y: 18
            }, {
                x: 63,
                y: 0
            }, {
                x: 49,
                y: 12
            }, {
                x: 35,
                y: 0
            }, {
                x: 28,
                y: 18
            }, {
                x: 11,
                y: 18
            }, {
                x: 17,
                y: 35
            }, {
                x: 0,
                y: 46
            }, {
                x: 17,
                y: 56
            }, {
                x: 11,
                y: 73
            }, {
                x: 28,
                y: 73
            }]
        }
        var o = s.map(function(e) {
            return fabric.util.object.clone(e)
        });
        var u = new fabric.Polygon(o, {
            left: 100,
            top: 50,
            fill: t,
            opacity: r,
            selectable: true,
            name: "Star",
            visible: true
        });
        var a = e("#shape-thickness-slider").val();
        if (a > 0) {
            u.set("strokeWidth", parseInt(a));
            u.set("stroke", n)
        }
        lt(u);
        w.add(u);
        w.centerObjectH(u);
        w.centerObjectV(u);
        w.renderAll();
        u.setCoords();
        Q()
    });
    e("#left_arrow").click(function() {
        w.deactivateAll();
        var t = w.getWidth();
        var n = w.getObjects();
        var r = 20;
        e.each(n, function(e, n) {
            var i = parseInt(n.get("left"));
            var s = 0 - parseInt(t) + n.get("width") / 2;
            var o;
            if (i >= s) o = i - r;
            else {
                o = t - (s - i - n.get("width") / 2)
            }
            n.set("left", parseInt(o));
            n.setCoords()
        });
        w.calcOffset();
        w.renderAll()
    });
    e("#right_arrow").click(function() {
        var t = w.getWidth();
        var n = w.getObjects();
        var r = 40;
        e.each(n, function(e, n) {
            var i = parseInt(t) + n.get("width") / 2;
            var s = parseInt(n.get("left"));
            if (s < i) {
                n.set("left", s + r)
            } else {
                var o = s - i;
                n.set("left", 0 - i + o + n.get("width"))
            }
        });
        w.renderAll()
    });
    e("#underline-cb").change(function() {
        var t = w.getActiveObject();
        if (t != null && t.type == "text") {
            var n = e("#underline-cb").is(":checked");
            if (n) t.set("textDecoration", "underline");
            else t.set("textDecoration", "none");
            w.renderAll()
        }
    });
    e("#bold-cb").change(function() {
        var t = w.getActiveObject();
        var n = e("#bold-cb").is(":checked");
        if (t != null && t.type == "text") {
            if (n) t.set("fontWeight", "bold");
            else t.set("fontWeight", "normal");
            w.renderAll()
        } else if (t != null && t.type == "group") {
            t.forEachObject(function(e) {
                if (n) e.set("fontWeight", "bold");
                else e.set("fontWeight", "normal");
                w.renderAll()
            })
        }
    });
    e("#italic-cb").change(function() {
        var t = w.getActiveObject();
        var n = e("#italic-cb").is(":checked");
        if (t != null && t.type == "text") {
            if (n) t.set("fontStyle", "italic");
            else t.set("fontStyle", "normal");
            w.renderAll()
        } else if (t != null && t.type == "group") {
            t.forEachObject(function(e) {
                if (n) e.set("fontStyle", "italic");
                else e.set("fontStyle", "normal");
                w.renderAll()
            })
        }
    });
    e("#font-family-selector").change(function() {
        var t = w.getActiveObject();
        var n = parseInt(e("#font-size-slider").val());
        var r = e(this).val();
        if (t != null && t.type == "text") {
            t.set("fontFamily", r);
            t.setFontSize(parseInt(n));
            w.renderAll()
        } else if (t != null && t.type == "group") {
            t.forEachObject(function(e) {
                e.set("fontFamily", r);
                e.setFontSize(parseInt(n));
                w.renderAll()
            })
        }
    });
    e("#font-size-slider").change(function() {
        var t = w.getActiveObject();
        var n = parseInt(e("#font-size-slider").val());
        if (t != null && t.type == "text") {
            t.setFontSize(parseInt(n));
            w.renderAll()
        } else if (t != null && t.type == "group") {
            q(t)
        }
    });
    e("#o-thickness-slider").change(function() {
        var t = w.getActiveObject();
        if (t != null && t.type == "text") {
            if (e(this).val() > 0) {
                var n = e("#txt-outline-color-selector").css("background-color");
                t.set("strokeWidth", parseInt(e(this).val()));
                t.set("stroke", n)
            } else t.set("stroke", false);
            w.renderAll()
        }
    });
    e("[id$='opacity-slider']").change(function() {
        var t = w.getActiveObject();
        if (t != null) {
            t.set("opacity", e(this).val());
            w.renderAll();
            Q()
        }
    });
    e("#curved-txt-radius-slider, #curved-txt-spacing-slider").change(function() {
        var e = w.getActiveObject();
        if (e != null && e.type == "group") q(e)
    });
    e("#img-cliparts-container").on("click", "img", function() {
        var t = e(this).attr("src");
        var n = e(this).data("price");
        z(t, n)
    });
    e(document).on("click", "#wpc-add-img", function(t) {
        t.preventDefault();
        var n = e(this).attr("data-selector");
        var r = e(this);
        var i = wp.media({
            title: "Add image on the design area",
            button: {
                text: "Add image"
            },
            multiple: false
        }).on("select", function() {
            var e = i.state().get("selection");
            e.map(function(e) {
                e = e.toJSON();
                z(e.url)
            })
        }).open()
    });
    e("#grayscale").change(function() {
        _(new fabric.Image.filters.Grayscale, e(this).is(":checked"))
    });
    e("#invert").change(function() {
        _(new fabric.Image.filters.Invert, e(this).is(":checked"))
    });
    e("#sepia").change(function() {
        _(new fabric.Image.filters.Sepia, e(this).is(":checked"))
    });
    e("#sepia2").change(function() {
        _(new fabric.Image.filters.Sepia2, e(this).is(":checked"))
    });
    e("#blur").change(function() {
        _(new fabric.Image.filters.Convolute({
            matrix: [1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9]
        }), e(this).is(":checked"))
    });
    e("#sharpen").change(function() {
        _(new fabric.Image.filters.Convolute({
            matrix: [0, -1, 0, -1, 5, -1, 0, -1, 0]
        }), e(this).is(":checked"))
    });
    e("#emboss").change(function() {
        _(new fabric.Image.filters.Convolute({
            matrix: [1, 1, 1, 1, .7, -1, -1, -1, -1]
        }), e(this).is(":checked"))
    });
    e(document).keydown(function(t) {
        var n = w.getActiveObject();
        var r = w.getActiveGroup();
        if (t.which == 46) e("#delete_btn").click();
        else if (t.which == 37) {
            if (r != null) {
                r.set("left", r.left - 1);
                w.renderAll();
                Q()
            } else if (n != null) {
                n.set("left", n.left - 1);
                w.renderAll();
                Q()
            }
        } else if (t.which == 39) {
            if (r != null) {
                r.set("left", r.left + 1);
                w.renderAll();
                Q()
            } else if (n != null) {
                n.set("left", n.left + 1);
                w.renderAll();
                Q()
            }
        } else if (t.which == 38) {
            if (r != null) {
                t.preventDefault();
                r.set("top", r.top - 1);
                w.renderAll();
                Q()
            } else if (n != null) {
                t.preventDefault();
                n.set("top", n.top - 1);
                w.renderAll();
                Q()
            }
        } else if (t.which == 40) {
            if (r != null) {
                t.preventDefault();
                r.set("top", r.top + 1);
                w.renderAll();
                Q()
            } else if (n != null) {
                t.preventDefault();
                n.set("top", n.top + 1);
                w.renderAll();
                Q()
            }
        } else if (t.keyCode == 67 && t.ctrlKey) {
            e("#copy_paste_btn").click()
        } else if (t.keyCode == 90 && t.ctrlKey) {
            e("#undo-btn").click()
        } else if (t.keyCode == 89 && t.ctrlKey) {
            e("#redo-btn").click()
        }
    });
    D();
    setTimeout(function() {
        w.calcOffset()
    }, 100);
    var x = [];
    var T = new Object;
    var N = -1;
    var C = [];
    var k = ["grayscale", "invert", "remove-white", "sepia", "sepia2", "brightness", "noise", "gradient-transparency", "pixelate", "blur", "convolute"];
    e(document).on("touchstart click", ".wpc-customize-product", function() {
        var t = 0;
        var n = e(this).data("type");
        if (n == "simple") t = e(this).data("id");
        else if (n == "variable") t = e("input[name='variation_id']").val();
        if (!t) {
            alert("Select the product options first");
            return
        } else {
            e.get(ajax_object.ajax_url, {
                action: "get_customizer_url",
                variation_id: t
            }, function(t) {
                if (t.url) e(location).attr("href", t.url)
            }, "json")
        }
    });
    e(document).on("touchstart click", ".wpc-upload-product-design", function(t) {
        t.preventDefault();
        var n = 0;
        var r = e(this).data("type");
        if (r == "simple") n = e(this).data("id");
        else if (r == "variable") n = e("input[name='variation_id']").val();
        if (!n) {
            alert("Select the product options first");
            return
        } else {
            e(".wpc-uploaded-design-container").show();
            e("#wpc-product-id-upl").val(n);
            e(this).hide()
        }
    });
    e(document).on("touchstart click", "#preview-order", function() {
        e("#wpc-modal .modal-body").html("");
        Q();
        st(v, V, function() {
            e("#wpc-parts-bar > span").first().click();
            e("#wpc-modal").modal("show");
            e.unblockUI()
        })
    });
    e(document).on("touchstart click", "#add-to-cart", function() {
        e("#debug").html("");
        Q();
        st(v, X, function() {
            if (jQuery.isEmptyObject(T)) {
                e("#debug").html("<div class='wpc-failure'>" + empty_object_msg + "</div>");
                e.unblockUI()
            } else {
                var t = e("#wpc-qty").val();
                var n = global_variation_id;
                var r = W("edit");
                if (typeof r == "undefined") r = "";
                e.post(ajax_object.ajax_url, {
                    action: "add_custom_design_to_cart",
                    variation_id: n,
                    cart_item_key: r,
                    final_canvas_parts: T,
                    quantity: t
                }, function(t) {
                    if (e("#wpc-parts-bar > span").length > 1) e("#wpc-parts-bar > span").first().click();
                    else J();
                    if (redirect_after == 1) {
                        e(location).attr("href", t.url)
                    } else {
                        e("#debug").html(t.message);
                        e.unblockUI()
                    }
                }, "json")
            }
        })
    });
    e(document).on("touchstart click", "#save-later", function() {
        st(v, X, function() {
            if (jQuery.isEmptyObject(T)) {
                e("#debug").html("<div class='wpc-failure'>" + empty_object_msg + "</div>");
                e.unblockUI()
            } else {
                var t = e("#wpc-qty").val();
                var n = global_variation_id;
                e.post(ajax_object.ajax_url, {
                    action: "save_custom_design_for_later",
                    variation_id: n,
                    final_canvas_parts: T,
                    quantity: t
                }, function(t) {
                    e.unblockUI();
                    e("#wpc-parts-bar > span").first().click();
                    if (!t.is_logged) e(location).attr("href", t.url);
                    else {
                        if (t.success) e(location).attr("href", t.url)
                    }
                }, "json")
            }
        })
    });
    e(".post-type-wpc-template #publish").click(function(t) {
        t.preventDefault();
        st(v, X, function() {
            if (jQuery.isEmptyObject(T)) {
                alert(empty_object_msg);
                e.unblockUI()
            } else {
                e.post(ajax_object.ajax_url, {
                    action: "save_canvas_to_session",
                    final_canvas_parts: T
                }, function(t) {
                    e("#wpc-parts-bar > span").first().click();
                    e("#post").submit();
                    e.unblockUI()
                })
            }
        })
    });
    e(document).on("touchstart click", "#download-design", function() {
        e("#debug").html("");
        st(v, X, function() {
            if (jQuery.isEmptyObject(T)) {
                e("#debug").html("<div class='wpc-failure'>" + empty_object_msg + "</div>");
                e.unblockUI()
            } else {
                var t = global_variation_id;
                e.post(ajax_object.ajax_url, {
                    action: "generate_downloadable_file",
                    final_canvas_parts: T,
                    variation_id: t
                }, function(t) {
                    if (e("#wpc-parts-bar > span").length > 1) e("#wpc-parts-bar > span").first().click();
                    else J();
                    e("#debug").html(t.message);
                    e.unblockUI()
                }, "json")
            }
        })
    });
    e(document).on("touchstart click", "#td-tools-container ul>li", function() {
        e("#td-tools-container ul>li").removeClass("selected");
        e(this).addClass("selected")
    });
    e(document).on("touchstart click", "#add-text", function() {
        e("#tools > div").hide();
        e("#text-tools").show()
    });
    e(document).on("touchstart click", "#saved-designs", function() {
        e("#tools > div").hide();
        e("#saved-designs-tools").show()
    });
    e(document).on("touchstart click", "#undo-btn", function() {
        var t = e("#wpc-parts-bar > span:eq(" + N + ")").attr("data-id");
        if (!e(this).hasClass("disabled") && C[t] > 0) {
            w.clear();
            C[t] --;
            w.loadFromJSON(x[t][C[t]]);
            K()
        }
    });
    e(document).on("touchstart click", "#redo-btn", function() {
        var t = e("#wpc-parts-bar > span:eq(" + N + ")").attr("data-id");
        if (!e(this).hasClass("disabled")) {
            w.clear();
            C[t] ++;
            w.loadFromJSON(x[t][C[t]]);
            K()
        }
    });
    e(".delete-design").click(function() {
        var t = e(this).data("index");
        var n = global_variation_id;
        var r = e(this);
        e.get(ajax_object.ajax_url, {
            action: "delete_saved_design",
            design_index: t,
            variation_id: n
        }, function(t) {
            if (t.success) {
                e(location).attr("href", t.url)
            } else alert(t.message)
        }, "json")
    });
    e("[data-original-title]").tooltip();
    e(document).on("change", "#wpc-qty", function() {
        var t = e(this).val();
        var n = e(this).attr("uprice");
        if (!e.isNumeric(t)) {
            e(this).val(1);
            e("#total_order").html(n);
            return
        }
        var r = n * t;
        e("#total_order").html(r)
    });
    e(".native-uploader #userfile").change(function() {
        var t = e(this).val().toLowerCase();
        if (t != "") {
            e("#userfile_upload_form").ajaxForm({
                success: Y
            }).submit()
        }
    });
    e(".native-uploader #user-custom-design").change(function() {
        var t = e(this).val().toLowerCase();
        if (t != "") {
            e("#custom-upload-form").ajaxForm({
                success: Z
            }).submit()
        }
    });
    var et = e("#userfile_upload_form.custom-uploader ul");
    if (e("#custom-upload-form.custom-uploader").length) {
        e("#custom-upload-form.custom-uploader").fileupload({
            dropZone: e("#drop"),
            add: function(t, n) {
                var r = e('<li class="working"><input type="text" value="0" data-width="48" data-height="48"' + ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');
                r.find("p").text(n.files[0].name).append("<i>" + nt(n.files[0].size) + "</i>");
                n.context = r.appendTo(et);
                r.find("input").knob();
                r.find("span").click(function() {
                    if (r.hasClass("working")) {
                        i.abort()
                    }
                    r.fadeOut(function() {
                        r.remove()
                    })
                });
                var i = n.submit()
            },
            progress: function(e, t) {
                var n = parseInt(t.loaded / t.total * 100, 10);
                t.context.find("input").val(n).change();
                if (n == 100) {
                    t.context.removeClass("working")
                }
            },
            fail: function(e, t) {
                t.context.addClass("error")
            },
            done: function(e, t) {
                Z(t.result, false, false, false)
            }
        })
    }
    e("#drop a").click(function() {
        e(this).parent().find("input").click()
    });
    if (e("#userfile_upload_form.custom-uploader").length) {
        e("#userfile_upload_form.custom-uploader").fileupload({
            dropZone: e("#drop"),
            add: function(t, n) {
                var r = e('<li class="working"><input type="text" value="0" data-width="48" data-height="48"' + ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');
                r.find("p").text(n.files[0].name).append("<i>" + nt(n.files[0].size) + "</i>");
                n.context = r.appendTo(et);
                r.find("input").knob();
                r.find("span").click(function() {
                    if (r.hasClass("working")) {
                        i.abort()
                    }
                    r.fadeOut(function() {
                        r.remove()
                    })
                });
                var i = n.submit()
            },
            progress: function(e, t) {
                var n = parseInt(t.loaded / t.total * 100, 10);
                t.context.find("input").val(n).change();
                if (n == 100) {
                    t.context.removeClass("working")
                }
            },
            fail: function(e, t) {
                t.context.addClass("error")
            },
            done: function(e, t) {
                var n = t.files[0].name;
                Y(t.result, false, false, false)
            }
        })
    }
    e(document).on("drop dragover", function(e) {
        e.preventDefault()
    });
    e(document).on("click", "#wpc-parts-bar > span", function(t) {
        var n = e(this).attr("data-url");
        if (N == e(this).index()) {
            return
        } else {
            it(e(this).index());
            e("#wpc-parts-bar > span").removeClass("active");
            e(this).addClass("active");
            if (N >= 0) {
                Q();
                w.clear()
            }
            N = e(this).index();
            if (n) {
                var r = "url('" + n + "') no-repeat center center";
                e("#wpc-editor-container").css("background", r)
            }
        }
        var i = e(this).attr("data-id");
        if (typeof x[i] == "undefined") {
            x[i] = [];
            C[i] = -1
        }
        if (x[i][C[i]]) {
            w.loadFromJSON(x[i][C[i]])
        }
        K()
    });
    var at = W("oid");
    if (typeof to_load == "undefined") {
        e("#wpc-parts-bar > span").each(function(t) {
            var n = e(this).attr("data-id");
            x[n] = [];
            C[n] = -1;
            var r = e("#wpc-parts-bar > span").length;
            if (t == r - 1) {
                st(v, ot, function() {
                    e("#wpc-parts-bar > span").first().click();
                    w.renderAll();
                    e.unblockUI()
                })
            }
        })
    }
    e("#image-tools-container").on("show", function() {
        ft()
    });
    e("#image-tools-container .AccordionPanelTab").click(function() {
        ft()
    });
    e(".scrollable").each(function() {
        e(this).perfectScrollbar({
            wheelSpeed: 100
        })
    });
    if (typeof to_load !== "undefined") setTimeout(function() {
        $(to_load)
    }, 500);
    e("#lock-mvt-x, #lock-mvt-y, #lock-scl-x, #lock-scl-y, #lock-Deletion").change(function(t) {
        var n = e(this).data("property");
        var r = w.getActiveObject();
        var i = w.getActiveGroup();
        if (r != null) {
            if (e(this).is(":checked")) r[n] = true;
            else r[n] = false;
            Q()
        } else if (i != null) {
            if (e(this).is(":checked")) i[n] = true;
            else i[n] = false;
            Q()
        }
    });
    e(document).on("click", "#wpc-qty-container .plus, #wpc-qty-container .minus", function() {
        var t = e("#wpc-qty"),
            n = parseFloat(t.val()),
            r = parseFloat(t.attr("max")),
            i = parseFloat(t.attr("min")),
            s = t.attr("step");
        if (!n || n === "" || n === "NaN") n = 0;
        if (r === "" || r === "NaN") r = "";
        if (i === "" || i === "NaN") i = 0;
        if (s === "any" || s === "" || s === undefined || parseFloat(s) === "NaN") s = 1;
        if (e(this).is(".plus")) {
            if (r && (r == n || n > r)) {
                t.val(r)
            } else {
                t.val(n + parseFloat(s))
            }
        } else {
            if (i && (i == n || n < i)) {
                t.val(i)
            } else if (n > 0) {
                t.val(n - parseFloat(s))
            }
        }
        t.trigger("change")
    });
    e(document).on("click", ".print_pdf", function(t) {
        t.preventDefault();
        var n = e(this).attr("href");
        var r = window.open(n)
    })
});
(function(e) {
    e.each(["show", "hide"], function(t, n) {
        var r = e.fn[n];
        e.fn[n] = function() {
            this.trigger(n);
            return r.apply(this, arguments)
        }
    })
})(jQuery)