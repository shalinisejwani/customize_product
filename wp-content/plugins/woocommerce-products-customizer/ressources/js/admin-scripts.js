jQuery(document).ready(function ($) {
    
    $(document).on("click",".wpc_img_upload",function(e){
        e.preventDefault();
       var selector=$(this).attr('data-selector');
       var uploader=wp.media({
           title:'Please set the picture',
           button:{
                    text:"Set Image"
                    },
            multiple:false
       })
       .on('select',function(){
            var selection=uploader.state().get('selection');
            selection.map(
                    function(attachment){
                            attachment=attachment.toJSON();
                            $("#"+selector).attr('value',attachment.id);
                            $("#"+selector+"_preview").html("<img src='"+attachment.url+"'>");
                    }
            )
        })
        .open();
   });
   
   //Cliparts add image
   $(document).on("click","#wpc-add-clipart",function(e){
        e.preventDefault();
       var selector=$(this).attr('data-selector');
       var trigger=$(this);
       var uploader=wp.media({
           title:'Please set the picture',
           button:{
                    text:"Set Image"
                    },
            multiple:true
       })
       .on('select',function(){
            var selection=uploader.state().get('selection');
            selection.map(
                    function(attachment){
                            attachment=attachment.toJSON();
                            var code="<input type='hidden' value='"+attachment.id+"' name='selected-cliparts[]'>";
                            code=code+"<span class='wpc-clipart-holder'><img src='"+attachment.url+"'>";
                            code=code+"<label>Price: <input type='text' value='0' name='wpc-cliparts-prices[]'></label>";
                            code=code+"<a href='#' class='button wpc-remove-clipart' data-id='"+attachment.id+"'>Remove</a></span>";
                            $("#cliparts-container").prepend(code);
                    }
            )
        })
        .open();
   });
   
   $(document).on("click",".wpc-remove-clipart",function(e){
       e.preventDefault();
      var id=$(this).data("id");
      $('#cliparts-form > input[value="'+id+'"]').remove();
      $(this).parent().remove();
   });
   
   $(".wpc_order_item").each(function(){
       var item_id=$(this).attr("data-item");
       $(this).insertBefore($("#order_items_list .item[data-order_item_id='"+item_id+"'] td.name table"));
   });
   
//   $('#clip_b_color').ColorPicker({
//                    color: ($('#clip_b_color').val()!=""?$('#clip_b_color').val():"#ffffff"),
//                    onShow: function (colpkr) {
//                            $(colpkr).fadeIn(500);
//                            return false;
//                    },
//                    onHide: function (colpkr) {
//                            $(colpkr).fadeOut(500);
//                            return false;
//                    },
//                    onChange: function (hsb, hex, rgb) {
//                            $('#clip_b_color').css('background-color', '#' + hex);
//                            $('#clip_b_color').val('#' + hex);
//                            
//                        }
//            });
   
   $(document).on("click","#wpc-customizer button",function(e){
       e.preventDefault();
   });
   
   $(document).on("change",".wpc-activate-part-cb",function(e){
       var is_checked=$(this).is(":checked");
       var selector=$(this).attr('data-selector');
       var output_area=selector+"_preview";
       if(is_checked)
           $("#"+selector).attr('value',0);
       else
           $("#"+selector).attr('value','');
       $("#"+output_area).html("");
           
   });
   
   $(document).on("click",".wpc_img_remove",function(e){
       e.preventDefault();
       var is_active=$(this).siblings(".wpc-activate-part-cb").is(":checked");
       var selector=$(this).attr('data-selector');
       var output_area=selector+"_preview";
       if(is_active)
           $("#"+selector).attr('value',0);
        else
           $("#"+selector).attr('value',"");
       
        $("#"+output_area).html("");
   });
   
   $('#wpc_parts_tab_data').on('show', function() {
      var post_id=$("#post_ID").val();
      var post_type=$("#product-type").val();      
      var variations_arr=new Object();
      $.each($(".woocommerce_variation h3"),function(){
         var elements=$(this).find("[name^='attribute_']");
         var attributes_arr=[];
         var variation_id=$(this).find('.remove_variation').first().attr("rel");
         $.each(elements,function(){
            attributes_arr.push($(this).val());
         });
         variations_arr[variation_id]=attributes_arr;                
      });
      
      $.post( 
                ajax_object.ajax_url,
                {
                    action: "get_wpc_product_tab_data_content",
                    product_id:post_id,
                    post_type:post_type,
                    variations:variations_arr
                },
                function(data) {
                    $("#wpc_parts_tab_data").html(data);
                }
            );
                
    });
    
    $('a[href*="post-new.php?post_type=wpc-template"]').click(function(e)
    {
       e.preventDefault(); 
       $('#wpc-products-selector-modal').modal("show");
    });
    
    $("#wpc-select-template").click(function(e){
        var selected_product=$( 'input[name=template_base_pdt]:checked' ).val();
        if(typeof selected_product == 'undefined')
            alert("Please select a product first");
        else
        {
            var url=$('a[href$="post-new.php?post_type=wpc-template"]').first().attr("href");
            $(location).attr('href',url+"&base-product="+selected_product);
        }
    });
    
    $("#wpc-settings .help_tip").each(function(i,e){
        var tip=$(e).data("tip");
       $(e).tooltip({title:tip});
    });
    
    $("#wpc-settings [name='wpc-color-palette']").change(function(){
        var palette=$(this).val();
        if(palette=="custom")
            $("#wpc-color-palette").show();
        else
            $("#wpc-color-palette").hide();
    });
    
    $(document).on("keyup","#wpc-settings [name='wpc-custom-palette[]']",function(e){
        var color=$(this).val();
        $(this).css("background-color",color);
    });
    
    $("#wpc-settings #wpc-add-color").click(function(e){
        e.preventDefault();
        var new_color='<div><input type="text" name="wpc-custom-palette[]"><button class="button wpc-remove-color">Remove</button></div>';
        $("#wpc-settings .wpc-colors").append(new_color);
    });
    
    $(document).on("click","#wpc-settings .wpc-remove-color",function(e){
        e.preventDefault();
        $(this).parent().remove();
    });
    
    $(document).on("click",".wpc-add-rule",function(e)
    {
        var new_rule_index=$(".wpc-rules-table tr").length;
        var group_index=$(this).data("group");
        var raw_tpl=$("#wpc-rule-tpl").val();
        var tpl1=raw_tpl.replace(/{rule-group}/g,group_index);
        var tpl2=tpl1.replace(/{rule-index}/g,new_rule_index);
        $(this).parents(".wpc-rules-table").find("tbody").append(tpl2);
        $(this).parents(".wpc-rules-table").find(".a_price").attr("rowspan",new_rule_index+1);
    });
    
    $(document).on("click",".wpc-add-group",function(e)
    {
        var new_rule_index=0;
        var group_index=$(".wpc-rules-table").length;
        var raw_tpl=$("#wpc-first-rule-tpl").val();
        var tpl1=raw_tpl.replace(/{rule-group}/g,group_index);
        var tpl2=tpl1.replace(/{rule-index}/g,new_rule_index);
        var html='<table class="wpc-rules-table widefat"><tbody>'+tpl2+'</tbody></table>';
        $(".wpc-rules-table-container").append(html);
    });
    
    $(document).on("click",".wpc-remove-rule",function(e)
    {
        var nb_rules=$(".wpc-rules-table tr").length;
        $(this).parents(".wpc-rules-table").find(".a_price").attr("rowspan",nb_rules-1);
        $(this).parents("tr").remove();
        
    });
});



//Triggers callbacks on hide/show
(function ($) {
    $.each(['show', 'hide'], function (i, ev) {
      var el = $.fn[ev];
      $.fn[ev] = function () {
        this.trigger(ev);
        return el.apply(this, arguments);
      };
    });
})(jQuery);
