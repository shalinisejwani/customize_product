<?php

require_once("../../../wp-config.php");
$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();
$wp->send_headers();
if(
    (isset($_POST['action'])&&$_POST['action']=="handle_picture_upload")
    ||
    (isset($_FILES['userfile']) && $_FILES['userfile']['error'] == 0)
  )
{
    $upload_dir=  wp_upload_dir();
    $generation_path = $upload_dir["basedir"];
    $generation_url = $upload_dir["baseurl"];
    $file_name=  uniqid();
    $valid_formats=  get_option("wpc-upl-extensions");
    if(!$valid_formats)
        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg");//wpc-upl-extensions
//    var_dump($valid_formats);
    $name = $_FILES['userfile']['name'];
    $size = $_FILES['userfile']['size'];
    
    if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
    {
    
    if(strlen($name))
    {
        list($txt, $ext) = explode(".", $name);
        $ext=  strtolower($ext);
        if(in_array($ext,$valid_formats))
        {
                $tmp = $_FILES['userfile']['tmp_name'];
                $success=0;
                $message="";
                if(move_uploaded_file($tmp, $generation_path."/".$file_name.".$ext"))
                {
                    $min_width=  get_option("wpc-min-upload-width");
                    $min_height=  get_option("wpc-min-upload-height");
                    if($min_width>0||$min_height>0)
                    {
                        list($width, $height, $type, $attr) = getimagesize($generation_path."/".$file_name.".$ext");
                        if(($min_width>$width||$min_height>$height)&&$ext!="svg")
                        {
                            $success=0;
                            $message=sprintf( __( 'Uploaded file dimensions: %1$spx x %2$spx, minimum required ', 'wpc' ), $width, $height );
                            if($min_width>0&&$min_height>0)
                                $message.="dimensions: $min_height"."px"." x $min_height"."px";
                            else if($min_width>0)
                                $message.="width: $min_width"."px";
                            else if($min_height>0)
                                $message.="height: $min_height"."px";
                        }
                        else
                        {
                            $success=1;
                            $message="<span class='clipart-img'><img src='$generation_url/$file_name.$ext'></span>";
                        }
                        
                    }
                    else                    
                    {
                        $success=1;
                        $message="<span class='clipart-img'><img src='$generation_url/$file_name.$ext'></span>";
                    }
                    if($success==0)
                        unlink ($generation_path."/".$file_name.".$ext");
                }
                else
                {
                    $success=0;
                    $message=__( 'An error occured during the upload. Please try again later', 'wpc' );
                }
        }
        else
        {
            $success=0;
            $message=__( 'Incorrect file extension. Allowed extensions: ', 'wpc' ).  implode(", ", $valid_formats);
        }
        echo json_encode(
                            array(
                                    "success"=>$success,
                                    "message"=>$message,
                            )
                        );
    }
    }
}

else if(isset($_POST['action'])&&$_POST['action']=="handle-custom-design-upload"
        ||
        (isset($_FILES['user-custom-design']) && $_FILES['user-custom-design']['error'] == 0))
{
    $upload_dir=  wp_upload_dir();
    $product_id=$_POST["wpc-product-id-upl"];
    $generation_path = $upload_dir["basedir"];
    $generation_url = $upload_dir["baseurl"];
    $file_name=  uniqid();
    $valid_formats=array();
    $valid_formats_raw=  get_option("wpc-custom-designs-extensions");
    if(!empty($valid_formats_raw))
    {
        $valid_formats=array_map('trim', explode(',', $valid_formats_raw));
    }
    $name = $_FILES['user-custom-design']['name'];
    $size = $_FILES['user-custom-design']['size'];
    
    if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
    {
    
    if(strlen($name))
    {
        if(!isset($_SESSION["wpc-user-uploaded-designs"]))
            $_SESSION["wpc-user-uploaded-designs"]=array();
        
        if(isset($_SESSION["wpc-user-uploaded-designs"][$product_id]))
            unset($_SESSION["wpc-user-uploaded-designs"][$product_id]);
        
        list($txt, $ext) = explode(".", $name);
        $ext=  strtolower($ext);
        if(in_array($ext,$valid_formats)||empty($valid_formats))
        {
//            var_dump($_FILES);
            $tmp = $_FILES['user-custom-design']['tmp_name'];
            $success=0;
            $message="";
            if(move_uploaded_file($tmp, $generation_path."/".$file_name.".$ext"))
            {
                $success=1;
                $_SESSION["wpc-user-uploaded-designs"][$product_id]="$generation_url/$file_name.$ext";
                $message=$_FILES['user-custom-design']['name']." successfully uploaded. Click on the Add to cart button to add this product and your design to the cart.";
            }
            else
            {
                $success=0;
                $message=__( 'An error occured during the upload. Please try again later', 'wpc' );
            }
        }
        else if(!in_array($ext,$valid_formats))
        {
            $success=0;
            $message=__( 'Incorrect file extension. Allowed extensions: ', 'wpc' ).  implode(", ", $valid_formats);            
        }
        echo json_encode(
                            array(
                                    "success"=>$success,
                                    "message"=>$message,
                            )
                        );
    }
    }
}

?>