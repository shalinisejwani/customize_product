<?php
add_action('init', 'register_cpt_template');
/**
 * Register the templates custom post type
 */
function register_cpt_template() {

    $labels = array(
        'name' => __('Templates', 'wpc-template'),
        'singular_name' => __('Template', 'wpc-template'),
        'add_new' => __('New template', 'wpc-template'),
        'add_new_item' => __('New template', 'wpc-template'),
        'edit_item' => __('Edit template', 'wpc-template'),
        'new_item' => __('New template', 'wpc-template'),
        'view_item' => __('View', 'wpc-template'),
        'search_items' => __('Search templates', 'wpc-template'),
        'not_found' => __('No template found', 'wpc-template'),
        'not_found_in_trash' => __('No template in the trash', 'wpc-template'),
        'menu_name' => __('Templates', 'wpc-template'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Templates for the products customizer.',
        'supports' => array( 'title','thumbnail' ),
        'public' => true,
        'menu_icon' => 'dashicons-media-default',
        'show_ui' => true,
        'show_in_menu' => false,
        'show_in_nav_menus' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => false,
        'can_export' => true
    );

    register_post_type('wpc-template', $args);
}

add_action( 'init', 'register_cpt_template_taxonomy', 0 );
/**
 * Register the templates categories
 */
function register_cpt_template_taxonomy()  {
        $labels = array(
                'name'                       => __( 'Categories', 'Taxonomy General Name', 'wpc' ),
                'singular_name'              => __( 'Category', 'Taxonomy Singular Name', 'wpc' ),
                'menu_name'                  => __( 'Categories', 'wpc' ),
                'all_items'                  => __( 'All templates categories', 'wpc' ),
        );
        $args = array(
                'labels'                     => $labels,
                'hierarchical'               => true,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
                'query_var'         => true,
        );
        register_taxonomy( 'wpc-template-cat', 'wpc-template', $args );

}

add_filter('manage_edit-wpc-template_columns', 'get_templates_columns');
function get_templates_columns($defaults) {
    $defaults['base_product'] =__('Base product','wpc');
    return $defaults;
}

add_action('manage_wpc-template_posts_custom_column', 'get_wpc_templates_columns_values', 5, 2);
function get_wpc_templates_columns_values($column_name, $id) {
    if ($column_name === 'base_product') {

        $base_product=  get_post_meta ($id,"base-product", true);
        $pdt_name=  get_the_title($base_product);
        $product=get_product($base_product);
        if($product->product_type=="variation")
            $link=  get_edit_post_link($product->parent->id);
        else
            $link= get_edit_post_link($base_product);
        echo "<a href='$link'>$pdt_name</a>";
    }
}

add_action( 'add_meta_boxes', 'get_wpc_template_metabox' );
/*
 * Adds the templates editor on the templates creation/edition page
 */
function get_wpc_template_metabox() {

    $screens = array( 'wpc-template' );

    foreach ( $screens as $screen ) {

            add_meta_box(
                    'wpc-template-box',
                    __( 'Template', 'wpc' ),
                    'get_wpc_template_metabox_content',
                    $screen
            );
    }
}

function get_wpc_template_metabox_content()
{
    $tmp_id=  get_the_ID();
    if(isset($_GET["base-product"]))
        $base_product=$_GET["base-product"];
    else
        $base_product=  get_post_meta ($tmp_id,"base-product", true);
    if(empty($base_product))
    {
        echo __("No base product found.","wpc");
        return;
    }
    ob_start();
    ?>
    <div class="wrap">
        <div id="wpc-template-container">
            <?php get_wpc_product_customizer($base_product,true);?>
        </div>
        <input type="hidden" name="base-product" value="<?php echo $base_product;?>">
    </div>
    <?php
    $output=  ob_get_contents();
    ob_end_clean();
    echo $output;

}

add_action("post_submitbox_misc_actions","get_templates_post_status");
/**
 * Displays the template status select
 */
function get_templates_post_status()
{
    if(get_post_type()=="wpc-template")
    {
    $statuses=array("publish", "draft");
    $current_status=get_post_status(get_the_ID());
    ?>
    <label class="mg-left-10">Status
        <select name="wpc-post-status" class="mg-bot-10">
            <?php
                foreach ($statuses as $status) {
                    if($current_status==$status)
                        echo '<option value="'.$status.'" selected>'.ucfirst ($status).'</option>';
                    else
                        echo '<option value="'.$status.'">'.ucfirst ($status).'</option>';
                }
            ?>
        </select>
    </label>
    <?php
    }
}

/**
 * Updates a post status
 * @param int $post_id Post ID 
 * @param type $status New status
 */
function wpc_change_post_status($post_id,$status){
    $current_post = get_post( $post_id, 'ARRAY_A' );
    $current_post['post_status'] = $status;
    wp_update_post($current_post);
}

add_action('save_post_wpc-template', 'save_wpc_template');
function save_wpc_template($post_id) {
    if(isset($_SESSION["to_save"]))
    {
        update_post_meta ($post_id, "data", $_SESSION["to_save"]);
        unset($_SESSION["to_save"]);
    }
    if(isset($_POST["base-product"]))
        update_post_meta ($post_id, "base-product", $_POST["base-product"]);
    
    if(isset($_POST["wpc-post-status"]))
    {
        remove_action('save_post_wpc-template', 'save_wpc_template');
        wpc_change_post_status($post_id, $_POST["wpc-post-status"]);
        add_action('save_post_wpc-template', 'save_wpc_template');
    }
}

add_action("admin_footer","get_product_selector");
/**
 * Build the base products popup when user wants to create a new template
 * @return type
 */
function get_product_selector()
{
    if((isset($_GET["post_type"]))&&!empty($_GET["post_type"]))
        $post_type=$_GET["post_type"];
    else
        $post_type=  get_post_type ();
    if($post_type!="wpc-template")
        return;
    $args=array(
                'numberposts' => -1,
                'post_type'        => 'product',
                'meta_query'=> array(
                                    array(
                                        'key' => 'customizable-product',
                                        'value' => '1'
                                        )
                                    )
                );
    $products= get_posts($args);
    
    ?>
    <div class="modal fade" id="wpc-products-selector-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 class="modal-title" id="myModalLabel">Which product would you like to use as base?</h3>
                </div>
                <div class="modal-body">
                   <?php
                   if(empty($products))
                       echo __("No customizable product found. You have to create at least one customizable product before creating a template.","wpc");
                   else
                   {
                        foreach ($products as $product)
                        {
                            $wc_product=get_product($product->ID);
                            if($wc_product->product_type=="variable")
                            {
                                $variations=$wc_product->get_available_variations();
                                foreach ($variations as $variation)
                                {
                                    $attributes_str=  implode(", ", $variation["attributes"]);
                                    echo '<div class="mg-bot-5"><label><input type="radio" name="template_base_pdt" value="'.$variation["variation_id"].'">'.$product->post_title." ($attributes_str)".'</label></div>';
                                }
                            }
                            else                            
                                echo '<div class="mg-bot-5"><label><input type="radio" name="template_base_pdt" value="'.$product->ID.'">'.$product->post_title.'</label></div>';
                        }
                        echo '<a class="button" id="wpc-select-template">'.__("Select","wpc").'</a>';
                   }
                   ?>
                    
                </div>
              </div>
            </div>
        </div>
    <?php
}

/**
 * Returns the template image url
 * @param int $template_id Template ID
 * @param string $size Preferred size. Default: Full
 * @return string
 */
function get_template_thumb($template_id=null, $size="full")
{
    $img_url=false;
    if(!$template_id)
        $template_id=  get_the_ID ();
    if (has_post_thumbnail($template_id)) {
            $thumb_id = get_post_thumbnail_id($template_id);
            $img_url = wp_get_attachment_url($thumb_id, $size);
    }
    return $img_url;
}

add_shortcode("wpc-templates", "get_wpc_templates");
function get_wpc_templates($atts)
{
    extract( shortcode_atts( array(
		'cat' => '',
                'products' => ''
	), $atts, 'wpc-templates' ) );
    $cat_ids=array();
    if(!empty($cat))
    {
        $cat_names=  explode(",",$cat);
        foreach ($cat_names as $cat_name) {
            $wpc_template_cat=  get_term_by("name", $cat_name, "wpc-template-cat");
            if($wpc_template_cat)
                array_push ($cat_ids, $wpc_template_cat->term_id);
        }
    }
    
    $paged=get_query_var('paged');
    $args=array(
            'post_type'=> 'wpc-template',
            'numberposts' => -1,
            'paged' => $paged,
            'posts_per_page'=>8,
            'post_status' => 'publish',
            'wpc-template-cat'=>$cat
            );
//    if(!empty($cat_ids))
//        $args['tax_query'] = array(
//                array(
//                    'taxonomy' => 'wpc-template-cat', 
//                    'field' => 'term_id', 
//                    'terms' => $cat_ids
//                )
//            );
    $product_arr=array();
    if(!empty($products))
    {
        $product_arr=  explode(",", $products);
        $args["meta_query"]= array(
                             array(
                                 'key' => "base-product",
                                 'value' => $product_arr,
                                 'compare' => "IN"
                                 )
                             );
    }
    $templates_query=new WP_Query($args);
    $output="<div id='wpc-templates-list'><div  class='wpc-grid wpc-grid-pad'>";
    $i=0;
    $nb_cols=3;
    while ($templates_query->have_posts())
    {
        $templates_query->the_post();
        $tpl_id=  get_the_ID ();
        $thumb_url=get_template_thumb();
        $base_product=  get_post_meta ($tpl_id,"base-product", true);
        $customize_url=  get_wpc_url($base_product)."&tpl=$tpl_id";
        if($i%$nb_cols==0)
            $output.="</div><div  class='wpc-grid wpc-grid-pad'>";
        $output.="<div class='wpc-col-1-$nb_cols'>
            <h3 class='wpc-tpl-title'>".get_the_title()."</h3>
            <img src='$thumb_url'>
                <a class='btn-choose mg-top-10' href='$customize_url'>".__("Use this template","wpc")."</a>
                </div>";
        $i++;

    }
    $output.="</div></div>";
    $output.=wpc_pagination($templates_query);
    wp_reset_postdata();
    return $output;
}

/**
 * Build the pagination used in the shortcodes
 * @global object $wp_rewrite
 * @param object $wp_query
 * @return string
 */
function wpc_pagination($wp_query) {
	
	global $wp_rewrite;
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
	
	$pagination = array(
		'base' => @add_query_arg('page','%#%'),
		'format' => '',
		'total' => $wp_query->max_num_pages,
		'current' => $current,
	        'show_all' => false,
	        'end_size'     => 1,
	        'mid_size'     => 2,
		'type' => 'list',
		'next_text' => '»',
		'prev_text' => '«'
	);
	
	if( $wp_rewrite->using_permalinks() )
		$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
	
	if( !empty($wp_query->query_vars['s']) )
		$pagination['add_args'] = array( 's' => str_replace( ' ' , '+', get_query_var( 's' ) ) );
		
	return str_replace('page/1/','', paginate_links( $pagination ) );
    }
    
    add_action('admin_init', 'register_tmp_object_locking_options', 1);
    function register_tmp_object_locking_options() {
        add_meta_box('wpc-tmp-locking-options', __('Locking options'), 'get_tmp_object_locking_options', 'wpc-template', 'side', 'default');
    }   

    function get_tmp_object_locking_options($product) {
        ?>
            <label><input type='checkbox' id='lock-mvt-x' data-property="lockMovementX" />Lock movement X</label><br>
            <label><input type='checkbox' id='lock-mvt-y' data-property="lockMovementY" />Lock movement Y</label><br>
            <label><input type='checkbox' id='lock-scl-x' data-property="lockScalingX" />Lock scaling X</label><br>
            <label><input type='checkbox' id='lock-scl-y' data-property="lockScalingY" />Lock scaling Y</label><br>
            <label><input type='checkbox' id='lock-Deletion' data-property="lockDeletion" />Lock deletion</label><br>
        <?php
    }
?>
