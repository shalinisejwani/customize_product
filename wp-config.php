<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpressshirts');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+cpD|K#v,lKQWPBJ?+:o] k}TNXAaU|ak+xMZo*m|^VVV7.4r}9 >TE<#Ag;cVe=');
define('SECURE_AUTH_KEY',  'cF#[FhxZS[S52Hd3V%%;Rr[$o8|D}jIHc;(ouSmsl6U3!tf3iB}4}QDhz-vS1K|H');
define('LOGGED_IN_KEY',    '#tq-hBz-6yT0B-OPF0%C{V+7@YO+T+(mrq.rgZ`iO^q*vKInG6d-zutyArB;|8|^');
define('NONCE_KEY',        '(Y3 8GpH~CqXl5|*z/OZq+wQ^d5_|K:+BFD@9X6s-x,.E73Fh;P;2JK4cyh|Rsy*');
define('AUTH_SALT',        'W~qJ#5ibA~6$b>+KGmb~6GvMZ-_EDa*LB{U_:$y!ApDzb.uLT~0q#*8|A}B%d_V#');
define('SECURE_AUTH_SALT', 'BgZ}-7df>tK6>B&<:;#*Y|.=[:Vk_75-|mg||!}|W #s*|PMhG!LT njYEu?B],$');
define('LOGGED_IN_SALT',   'I8/-U*(y-jHZ9hQcD;e_:QS%IL[O](PNHT>3ei$`!mKHb-YEjx=mwA5#E:o.yvAH');
define('NONCE_SALT',       '?kg7 c7L!h]Zu.BlI=$y>/7)C),i+0i+J92FslD8-V~qlav`}m7/Uw+^}=BA+F%}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
